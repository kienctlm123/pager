package com.chao;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by QuanLH7 on 7/28/2015.
 */
public class ApdaterListView extends BaseAdapter {

    ArrayList<ObItemListView> myList = new ArrayList<ObItemListView>();
    LayoutInflater inflater;
    Context context;


    int mSelectedPos = -1;

    public ApdaterListView(Context context, ArrayList<ObItemListView> myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public ObItemListView getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        ObItemListView currentListData = getItem(position);


        mViewHolder.ivIcon.setImageResource(currentListData.getImgView());


        return convertView;
    }

    private class MyViewHolder {
        TextView tvTitle,txtDay;
        ImageView ivIcon;

        public MyViewHolder(View item) {

            ivIcon = (ImageView) item.findViewById(R.id.imgItem);
        }
    }

    public int getSelectedPos() {
        return mSelectedPos;
    }

    public void setSelectedPos(int mSelectedPos) {
        this.mSelectedPos = mSelectedPos;
    }

}