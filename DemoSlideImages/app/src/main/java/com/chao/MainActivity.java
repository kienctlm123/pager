package com.chao;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;


public class MainActivity extends Activity {
    private ImageView img1, img2, img3, img4, img5;
    private DiscreteSeekBar seekBar;
    private ExtendedViewPager viewPager;
    private HorizontalListView listView;
    private ArrayList<ObItemListView> arrayList = new ArrayList<>();
    private int[] mGalImages = new int[]{R.drawable.hinh1, R.drawable.hinh2,
            R.drawable.hinh3, R.drawable.hinh4, R.drawable.hinh5, R.drawable.hinh6, R.drawable.hinh7
            , R.drawable.hinh8, R.drawable.hinh9, R.drawable.hinh10, R.drawable.hinh11, R.drawable.hinh12, R.drawable.hinh1, R.drawable.hinh2,
            R.drawable.hinh3, R.drawable.hinh4, R.drawable.hinh5, R.drawable.hinh6, R.drawable.hinh7
            , R.drawable.hinh8, R.drawable.hinh9, R.drawable.hinh10, R.drawable.hinh11, R.drawable.hinh12};

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitInterface();

        ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();
        for (int i = 0; i < mGalImages.length; i++) {
            bitmaps.add(decodeSampledBitmapFromResource(getResources(), mGalImages[i], 250, 250));
        }
        viewPager.setAdapter(new TouchImageAdapter(this, mGalImages));
        for (int i = 0; i < mGalImages.length; i++) {
            ObItemListView obItemListView = new ObItemListView();
            obItemListView.setImgView(mGalImages[i]);
            arrayList.add(obItemListView);

        }
        Log.d("Test", "+arrayList" + arrayList.size());
        ApdaterListView apdaterListView = new ApdaterListView(getApplication(), arrayList);
        listView.setAdapter(apdaterListView);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {
                seekBar.setProgress(position * 10 + 5);
            }
        });
        //

        seekBar.setMax(mGalImages.length * 10);
        seekBar.setProgress(5);
        seekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {
                return value / 10;
            }
        });
        seekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            int progressChanged = 5;

            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int progress, boolean b) {
                if (progress < 5) {
                    progress = 5;
                } else {
                    if (progress % 10 >= 5)
                        progress = (progress / 5) * 5;
                    else
                        progress = (progress / 5) * 5 + 5;

                }
                seekBar.setProgress(progress);
                progressChanged = progress;
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
                viewPager.setCurrentItem(progressChanged / 10, true);
            }
        });


    }


    /**
     * Init Interface
     */
    public void InitInterface() {
        viewPager = (ExtendedViewPager) findViewById(R.id.view_pager);
        listView = (HorizontalListView) findViewById(R.id.horizoltal_listview);
        seekBar = (DiscreteSeekBar) findViewById(R.id.seekBar);
    }
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }
//
 class TouchImageAdapter extends PagerAdapter {
    private Context context;
    private int[] mGalImages;
    TouchImageAdapter(Context context, int[] mGalImages) {
        this.context = context;
        this.mGalImages = mGalImages;
    }
    @Override
    public int getCount() {
        return mGalImages.length;
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        TouchImageView img = new TouchImageView(container.getContext());
        img.setImageResource(mGalImages[position]);
        container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        return img;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
}
